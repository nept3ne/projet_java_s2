package moduleGestion;

import java.io.Serializable;

public abstract class Zone implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int longueur;
	private int largeur;
	@SuppressWarnings("unused")
	private boolean occupe;
	
	public Zone(int lon, int lar){
		longueur = lon;
		largeur = lar;
	}
	
	public int getLongueur() {
		return longueur;
	}
	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}
	public int getLargeur() {
		return largeur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	
	public String toString() {
		return longueur + " x " + largeur;
	}
	
	
}

