package moduleGestion;

import java.io.Serializable;

import javafx.scene.paint.Color;

public class PointEau extends Zone implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int nb_robinet;
	private final static Color couleur = Color.BLUE;
	private final static int largeur =1;
	private final static int longueur = 1;
	@SuppressWarnings("unused")
	private int longitude;
	private int latitude;

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public int getLongueur() {
		return longueur;
	}

	public PointEau(int n, int longi, int lati){
		super(largeur, longueur);
		nb_robinet = n;
	}
	
	public int getNb_robinet() {
		return nb_robinet;
	}

	public void setNb_robinet(int nb_robinet) {
		this.nb_robinet = nb_robinet;
	}

	public static Color getCouleur() {
		return couleur;
	}

	public String toString() {
		return "Point d'eau: nombre=" + nb_robinet + ", taille=" + super.toString();
	}
	
}
