package moduleGestion;

import java.io.Serializable;

public class Stand extends Zone implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numStand;
	private final static String couleur = "Orange";
	private int longitude;
	private int latitude;
	
	public Stand(int n, int longi, int lati) {
		super(1, 1);
		numStand = n;
	}

	public static String getCouleur() {
		return couleur;
	}
	
	public int getNumStand() { 
		return numStand;
	}

	public int getLongitude() {
		return longitude;
	}

	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public void setNumStand(int numStand) {
		this.numStand = numStand;
	}

	public String toString() {
		return "Stand : numero=" + numStand + "taille="+ super.toString();
	}
	
}

