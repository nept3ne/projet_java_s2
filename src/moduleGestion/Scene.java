package moduleGestion;

import java.io.Serializable;

public class Scene extends Zone implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int getLongitude() {
		return longitude;
	}

	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
	private String nom;
	private final static String couleur = "Green";
	private final static int prix=1000;
	private int longitude;
	private int latitude;
	
	
	public Scene(int lon, int lar, String n, int longi, int lati){
		super(lon, lar);
		nom = n;
		longitude = longi;
		latitude = lati;
	}
	
	public static String getCouleur() {
		return couleur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String toString() {
		return "nom=" + nom + ", taille=" + super.toString();
	}
	public float calculerPrix() {
		float taille = this.getLargeur()*this.getLongueur();
		return taille*prix;
	}
	
	
}

