package moduleGestion;

import java.io.Serializable;

public class Artiste implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pseudo;
	private int tarif;
	
	public Artiste(String p, int t){
		pseudo = p;
		tarif = t;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public int getTarif() {
		return tarif;
	}

	public void setTarif(int tarif) {
		this.tarif = tarif;
	}

	public String toString() {
		return "pseudo=" + pseudo + ", tarif=" + tarif;
	}
	
			
}
