package moduleGestion;

import java.io.Serializable;

public class Terrain implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Création des constantes
	private final static double prix = 0.314;
	private final static int hCase = 5;
	private final static int lCase = 5;
	
	//création des variables
	private int longueur;
	private int largeur;
	private double coutLocation;
	private int taille;
	private int nbSc;
	private int nbSt;
	private int nbPe;
	private int nbPr;
	private int minSc;
	private int minSt;
	private int minPe;
	private int minPr;
	
	//constructeur
	public Terrain(int lon, int lar) {
		longueur = lon;
		largeur = lar;
		coutLocation = calculCoutLoc();
		taille = lon*lar;
		minSc = taille/33;
		minSt = taille/66;
		minPe = minSt;
		minPr = minSt;
	}

	//Fonction pour la validité de la création du terrain, permettant de dégriser ou non le bouton valider
	public boolean validActiv(){
		return minSc <= nbSc && minSt <= nbSt && minPe <= nbPe && minPr <= nbPr;
	}
	
	//Fonction calculant le coût de location du terrain
	public double calculCoutLoc(){
		return longueur*largeur*prix;
	}
	
	//Ensemble des getter, pour pouvoir afficher les valeurs sur l'interface graphique
	public static double gethCase() {
		return hCase;
	}

	public static int getLcase() {
		return lCase;
	}
	
	public int getNbCase() {
		return taille;
	}

	public int getNbSc() {
		return nbSc;
	}

	public int getNbSt() {
		return nbSt;
	}

	public int getNbPe() {
		return nbPe;
	}

	public int getNbPr() {
		return nbPr;
	}

	public int getMinSc() {
		return minSc;
	}

	public int getMinSt() {
		return minSt;
	}

	public int getMinPe() {
		return minPe;
	}

	public int getMinPr() {
		return minPr;
	}

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public double getCoutLocation() {
		return coutLocation;
	}

	public void setCoutLocation() {
		this.coutLocation = calculCoutLoc();
	}

	public String toString() {
		return "Terrain : taille=" + longueur + "x" + largeur + ", coutLocation=" + coutLocation;
	}
	
}
