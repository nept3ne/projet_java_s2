package moduleGestion;

import java.io.Serializable;
import java.util.*;

import moduleCompta.*;

public class Festival implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String nom;
	private static String ville;
	private Terrain terrainFest;
	private Planning planningFest;
	private Comptabilite compta;
	private ArrayList<Planning> liste_planning;
	private Placement placement = new Placement();
	
	public Festival(String n, String v) {
		nom = n;
		ville = v;
		liste_planning = new ArrayList<Planning>();
		
	}

	public static String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		Festival.nom = nom;
	}

	public static String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
	
	public Terrain getTerrainFest() {
		return terrainFest;
	}

	public void setTerrainFest(Terrain terrainFest) {
		this.terrainFest = terrainFest;
	}

	public Planning getPlanningFest() {
		return planningFest;
	}

	public void setPlanningFest(Planning planningFest) {
		this.planningFest = planningFest;
	}

	public Comptabilite getCompta() {
		return compta;
	}

	public void setCompta(Comptabilite compta) {
		this.compta = compta;
	}

	public ArrayList<Planning> getListe_planning() {
		return liste_planning;
	}

	public void setListe_planning(ArrayList<Planning> liste_planning) {
		this.liste_planning = liste_planning;
	}

	public String toString() {
		return "Festival : nom=" + nom + ", ville=" + ville;
	}
	public Placement getPlacement() {
		return placement;
	}

}

