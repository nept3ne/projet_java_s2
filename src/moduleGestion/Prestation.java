package moduleGestion;

import java.io.Serializable;

public class Prestation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int jour;
	private int heure;
	private Artiste artiste;
	private Scene scene;
	private boolean dispo;
	
	public Prestation(int j, int h){
		jour = j;
		heure = h;
		dispo = true;
		artiste = null;
	}
	
	public Prestation(int j, int h, String a, boolean d) {
		jour = j;
		heure = h;
		dispo = d;
		artiste.setPseudo(a);
	}

	public void EditArtiste(Artiste a) {
		artiste = a;
	}
	
	public void EditScene(Scene s) {
		scene = s;
	}

	public Artiste getArtiste() {
		return artiste;
	}

	public boolean isDispo() {
		return dispo;
	}
	
	public Scene getScene() {
		return scene;
	}
	
	public int getJour() {
		return jour;
	}

	public void setJour(int jour) {
		this.jour = jour;
	}

	public int getHeure() {
		return heure;
	}

	public void setHeure(int heure) {
		this.heure = heure;
	}

	public String toString() {
		return "Prestation : jour=" + jour + ", heure=" + heure + ", artiste=" + artiste+", dispo="+dispo;
	}
}
	
	
