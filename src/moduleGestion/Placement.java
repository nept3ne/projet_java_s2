package moduleGestion;

import java.io.Serializable;
import java.util.ArrayList;

public class Placement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Stand> arrayStand;
	private ArrayList<PointEau> arrayPtEau;
	private ArrayList<Scene> arrayScene;
	private Terrain terrainFestival;
	
	Placement(Placement pla){
		this.arrayScene = pla.arrayScene;
		arrayStand=pla.arrayStand;
		arrayScene = pla.arrayScene;
		terrainFestival = pla.terrainFestival;
	}
	
	Placement() {
		arrayStand = new ArrayList<Stand>(100);
		arrayPtEau = new ArrayList<PointEau>(100);
		arrayScene = new ArrayList<Scene>(100);
	}

	public ArrayList<Stand> getArrayStand() {
		return arrayStand;
	}

	public void setArrayStand(ArrayList<Stand> arrayStand) {
		this.arrayStand = arrayStand;
	}

	public ArrayList<PointEau> getArrayPtEau() {
		return arrayPtEau;
	}

	public void setArrayPtEau(ArrayList<PointEau> arrayPtEau) {
		this.arrayPtEau = arrayPtEau;
	}

	public ArrayList<Scene> getArrayScene() {
		return arrayScene;
	}

	public void setArrayScene(ArrayList<Scene> arrayScene) {
		this.arrayScene = arrayScene;
	}
}
