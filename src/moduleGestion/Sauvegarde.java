package moduleGestion;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import moduleGraphique.Main;

public class Sauvegarde {
	public Sauvegarde(){
	}
	
	XMLEncoder encoder = null;
	XMLDecoder decoder = null;
	
	public void save(Object obj, String nomF) {
		try {
		      encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("personne.xml")));
		      encoder.writeObject(obj);
		      encoder.flush();
		    } catch (final java.io.IOException e) {
		      e.printStackTrace();
		    } finally {
		      if (encoder != null) {
		        encoder.close();
		      }
		    }
	}
	public Object restore(String nomF){
//---------- Important : cette fonction retourne un objet, il faut donc caster cette fonction vers le type d'objet voulu
			
			try {
		      decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("personne.xml")));
		      final Object obj = decoder.readObject();
		      System.out.println(obj);
		      return obj;
		    } catch (final Exception e) {
		      e.printStackTrace();
		      return null;
		    } finally {
		      if (decoder != null) {
		        decoder.close();
		      }
		    }
			
	}
	public void sauverFestival(String nomF) {
		this.save(Main.getFestival(), nomF);
	}
	public void restoreFestival(String nomF) throws Exception {
		Festival a = (Festival) restore(nomF);
		Main.setFestival(a);
	}
}
