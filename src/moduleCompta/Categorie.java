package moduleCompta;

public abstract class Categorie{
	private String nom;
	private int montant;
	
	public Categorie(String n, String m) {
		// Exception si montant invalide
		montant = Integer.parseInt(m);
		nom = n;		
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getMontant() {
		return montant;
	}
	public void setMontant(int montant) {
		this.montant = montant;
	}
	
	
	
}
