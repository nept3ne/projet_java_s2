package moduleCompta;

public abstract class CategorieType {
	private float tarif;
	private String nom;
	CategorieType(float t, String n){
		this.tarif = t;
		this.nom = n;
	}
	public abstract float totalPrix();
	
	//getters and setters
	
	public float getTarif() {
		return tarif;
	}
	public void setTarif(float tarif) {
		this.tarif = tarif;
		
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
}
