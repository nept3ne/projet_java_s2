package moduleGraphique;


import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;
import moduleGestion.*;

public class FenetrePlanning extends Stage{
	//formes pour la partie superieur du borderPane
		private Rectangle rectTop1 = new Rectangle();
		private Rectangle rectTop2 = new Rectangle();
		private Rectangle rectTop3 = new Rectangle();
		private Text txtTop3 = new Text();

	// formes pour la partie inferieur du borderPane 
		private Rectangle rectBot1 = new Rectangle();
		private Rectangle rectBot2 = new Rectangle();
		private Rectangle rectBot3 = new Rectangle();
		
		//creation partie 1 du centre
		private Label titre = new Label("Planning : ");
		private Label txt1 = new Label("Date de debut du festival : ");
		private Label txt2 = new Label("Date de fin du festival : ");
		private Label txt3 = new Label("Heure de debut du festival : ");
		private Label txt4 = new Label("Heure de fin du festival : ");
		
		private Button bnVal = new Button("Valider");
		
		private Label txtSc = new Label("Scene(s) : ");
		private ComboBox<String> nbSc = new ComboBox<String>();
		
		private GridPane tab = new GridPane();
		
		
		//private LectureFichierPlanning fic = new LectureFichierPlanning();
		private Planning liste; //= fic.getliste();
		
		private Button ajoutArtiste = new Button("Ajouter Artiste");
		
	//texte du panneau central
		private Text textBot2My = new Text("Maryne G");
		private Text textBot2Ma = new Text("Matthis G");
		private Text textBot2Ql = new Text("Quentin L");
		private Text textBot2Cd = new Text("Corentin DR");
		
	//Autres constantes
		private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
		private final int largeurRectangleSup=300; // defini la largeur de chaque rectangles superieurs
		private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
		private final int largeurRectangleInf=300;// defini la largeur de chaque rectangles inferieur
		private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
		private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
	//Variables d initialisation de la fen�tre + bord superieur et inferieur
		private final int largeurRectangleBot_Cent = 500; // largeur du rectangle inferieur centre
		private final int hauteurRectangleBot_Cent = 50; // hauteur du rectangle inferieur centre
		
		private int widthFenPrin=1000; //defini la largeur de la fentre principal
		private int heightFenPrin=600; // defini la hauteur de la fenetre principal
		private boolean tailleChangeable = false; // autorise le changement de la taille de la fenetre par l'utilisateur
		
		public FenetrePlanning() {
			
			this.setTitle("Festiv'Org");
			this.setResizable(false);
			this.setX(200);
			this.setY(200);
			Scene SceneAcc = new Scene(plaContenu());
			this.setScene(SceneAcc);
			this.setResizable(tailleChangeable);
			this.setWidth(widthFenPrin);
			this.setHeight(heightFenPrin);
			this.sizeToScene();
			
		}

		private Parent plaContenu() {
			
			// --------------------------------------------------------------------------------------------------------------------------------------------------
			// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
			HBox topRect = new HBox();
			StackPane topStackPane = new StackPane();		

			rectTop1.setFill(Color.GREY);
			rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
			rectTop3.setFill(Color.GREY);


			rectTop1.setWidth(largeurRectangleSup);
			rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
			rectTop3.setWidth(largeurRectangleSup);

			rectTop1.setHeight(hauteurRectangleSup);
			rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
			rectTop3.setHeight(hauteurRectangleSup);

			rectTop1.setStroke(Color.BLACK);
			rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
			rectTop3.setStroke(Color.BLACK);

			rectTop1.setStrokeWidth(largStrokTop);
			rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
			rectTop3.setStrokeWidth(largStrokTop);

			txtTop3.setText("Festiv'Org"); // fixe le texte superieur
			topStackPane.getChildren().addAll(rectTop2, txtTop3);

			topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);

			//-----------------------------------------------------------------------------------------------------------------------------------------------------
			//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------

			HBox botRect1 = new HBox();

			FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
			nomPane.setAlignment(Pos.CENTER);
			nomPane.setColumnHalignment(HPos.CENTER);

			nomPane.setPrefWidth(largeurRectangleBot_Cent);
			nomPane.setPrefHeight(hauteurRectangleBot_Cent);
			nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);



			rectBot1.setFill(Color.GREY);
			rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
			rectBot3.setFill(Color.GREY);

			rectBot1.setWidth(largeurRectangleInf);
			rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
			rectBot3.setWidth(largeurRectangleInf);

			rectBot1.setHeight(hauteurRectangleInf);
			rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
			rectBot3.setHeight(hauteurRectangleInf);

			rectBot1.setStroke(Color.BLACK);
			rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
			rectBot3.setStroke(Color.BLACK);

			rectBot1.setStrokeWidth(largStrokeBot);
			rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
			rectBot3.setStrokeWidth(largStrokeBot);

			StackPane nomStack = new StackPane();
			nomStack.setPrefWidth(largeurRectangleBot_Cent);
			nomStack.setMaxWidth(hauteurRectangleBot_Cent);
			nomStack.getChildren().addAll(rectBot2, nomPane);

			botRect1.getChildren().addAll(rectBot1,nomStack,rectBot3);
			
			//-----------------------------------------------------------------------------------------------------------------------------------------------------
			//----------------------------Ajustement de la partie centrale-----------------------------------------------------------------------------
			
			liste = new Planning(null);
			
			
			VBox cenRect = new VBox();
			HBox boxDate = new HBox();
			HBox boxHeure = new HBox();
			VBox boxDateHeure = new VBox();
			HBox boxScene = new HBox();
			
			cenRect.setAlignment(Pos.BASELINE_CENTER);
			boxDate.setSpacing(40);
			boxDate.setAlignment(Pos.BASELINE_CENTER);
			
			boxHeure.setSpacing(40);
			boxHeure.setAlignment(Pos.BASELINE_CENTER);
			boxDateHeure.getChildren().addAll(boxDate, boxHeure);
			cenRect.setSpacing(20);
			
			
			
			
			for(int i = 0; i>liste.getNb_jours(); i++){
				Label fen = new Label(liste.getListe_prestations().get(i).toString());
				tab.add(fen, liste.getListe_prestations().get(i).getHeure(), liste.getListe_prestations().get(i).getJour());
			}
			
			boxDate.getChildren().addAll(txt1, txt2);
			boxHeure.getChildren().addAll(txt3, txt4);
			boxScene.getChildren().addAll(txtSc, nbSc);
			cenRect.getChildren().addAll(titre, boxDateHeure, bnVal, boxScene, tab, ajoutArtiste);
			
			
			
			BorderPane root = new BorderPane();
			root.setTop(topRect);
			root.setCenter(cenRect);
			root.setBottom(botRect1);
			return root;
		}
		
}
