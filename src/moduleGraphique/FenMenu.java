package moduleGraphique;

import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;

public class FenMenu extends Stage {
	
		
	private Button Placement = new Button("Gestion du placement");
	private Button equipement = new Button("Gestion des �quipements");
	private Button ventes = new Button("Gestion des ventes");
	private Button planning = new Button("Planning");
//	private Main resolution = new Main();
	
	//formes pour la partie superieur du borderPane
		private Rectangle rectTop1 = new Rectangle();
		private Rectangle rectTop2 = new Rectangle();
		private Rectangle rectTop3 = new Rectangle();
		private Text txtTop3 = new Text();
		// formes pour la partie inferieur du borderPane 
		private Rectangle rectBot1 = new Rectangle();
		private Rectangle rectBot2 = new Rectangle();
		private Rectangle rectBot3 = new Rectangle();
		private Button butBot3 = new Button("Fermer");
		private Button butBot4 = new Button("Purger l'�v�nement");
		
		private Text textBot2My  = new Text("Maryne G");
		private Text textBot2Ma = new Text("Matthis G");
		private Text textBot2Ql = new Text("Quentin L");
		private Text textBot2Cd = new Text("Corentin DR");
			
		
		//Variables d initialisation de la fen�tre + bord superieur et inferieur
		private int widthFenPrin=900; //defini la largeur de la fentre principal
		private int heightFenPrin=600; // defini la hauteur de la fenetre principal
		private boolean tailleChangeable = false; // autorise le changement de la taille de la fenetre par l'utilisateur
		private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
		private final int largeurRectangleSup=300; // defini la largeur de chaque rectangles superieurs
		private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
		private final int largeurRectangleInf=300;// defini la largeur de chaque rectangles inferieur
		private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
		private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
		private final String cheminRessources="file:ressources/fonts/Montserrat-Regular.ttf"; // defini le chemin vers la police Montserrat
		private final int largeurRectangleBot_Cent = 200; // largeur du rectangle inferieur centre
		private final int hauteurRectangleBot_Cent = 70; // hauteur du rectangle inferieur centre
		private final int hauteurBoutonBut3 = 30;
		private final int largeurBoutonBut3 = 100;
		
		// elements de la zone principal de la fenetre :
		// autres de la zone principal
		private Rectangle bground;
		private Text titre ;
		private int tailleTextTitre=25;
	
	public FenMenu() {
		this.setTitle("Festiv'Org");
		this.setX(200);
		this.setY(200);
		Scene SceneAcc = new Scene(accContenu());
		this.setScene(SceneAcc);
		this.setResizable(tailleChangeable);
		this.setWidth(widthFenPrin);
		this.setHeight(heightFenPrin);
		this.sizeToScene();
		
	
	}

	private Parent accContenu() {
		// --------------------------------------------------------------------------------------------------------------------------------------------------
		// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
		HBox topRect = new HBox();
		StackPane topStackPane = new StackPane();		
		
		rectTop1.setFill(Color.GREY);
		rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
		rectTop3.setFill(Color.GREY);
		
		
		
		
		rectTop1.setWidth(largeurRectangleSup);
		rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
		rectTop3.setWidth(largeurRectangleSup);

		
		rectTop1.setHeight(hauteurRectangleSup);
		rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
		rectTop3.setHeight(hauteurRectangleSup);
		
		rectTop1.setStroke(Color.BLACK);
		rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
		rectTop3.setStroke(Color.BLACK);
		
		rectTop1.setStrokeWidth(largStrokTop);
		rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
		rectTop3.setStrokeWidth(largStrokTop);

		txtTop3.setText("Festiv'Org"); // fixe le texte superieur
		topStackPane.getChildren().addAll(rectTop2, txtTop3);
		
		topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);
		
//-----------------------------------------------------------------------------------------------------------------------------------------------------
		//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------
		
		HBox botRect = new HBox();
		
		FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
		nomPane.setAlignment(Pos.CENTER);
		nomPane.setColumnHalignment(HPos.CENTER);
		
		nomPane.setPrefWidth(largeurRectangleBot_Cent);
		nomPane.setPrefHeight(hauteurRectangleBot_Cent);
		nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);
		
		
		rectBot1.setFill(Color.GREY);
		rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
		rectBot3.setFill(Color.GREY);

		rectBot1.setWidth(largeurRectangleInf);
		rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
		rectBot3.setWidth(largeurRectangleInf);
		
		rectBot1.setHeight(hauteurRectangleInf);
		rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
		rectBot3.setHeight(hauteurRectangleInf);
		
		rectBot1.setStroke(Color.BLACK);
		rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
		rectBot3.setStroke(Color.BLACK);
		
		rectBot1.setStrokeWidth(largStrokeBot);
		rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
		rectBot3.setStrokeWidth(largStrokeBot);
		
		butBot3.setPrefWidth(largeurBoutonBut3);
		butBot3.setPrefHeight(hauteurBoutonBut3);
		
		planning.setOnAction(e->{
			this.close();
			Main.afficherPlanning();
			
		});
		
		butBot3.setOnAction(e->{
			this.close();
		});
		
		
//		ventes.setOnAction(e->{
//			ac.afficheplusinfo();
//		});
		
		Placement.setOnAction(e->{Main.ouvrirPlacement();});
		
		StackPane btnStack = new StackPane();
		btnStack.setPrefWidth(hauteurRectangleInf);
		btnStack.setMaxWidth(largeurRectangleInf);
		btnStack.getChildren().addAll(rectBot3, butBot3);
		
		
		StackPane nomStack = new StackPane();
		
		nomStack.setPrefWidth(largeurRectangleBot_Cent);
		nomStack.setMaxWidth(hauteurRectangleBot_Cent);
		nomStack.getChildren().addAll(rectBot2, nomPane);
		
		StackPane btnPurg = new StackPane();
		btnPurg.setPrefWidth(hauteurRectangleInf);
		btnPurg.setMaxWidth(largeurRectangleInf);
		btnPurg.getChildren().addAll(rectBot1, butBot4);
		
		botRect.getChildren().addAll(btnPurg,nomStack,btnStack);
//----------------------------------------------------------Fin bord superieur et inferieur------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		HBox centreHbox = new HBox();
		VBox vBoxGauche = new VBox();
		VBox vBoxDroite = new VBox();
		
		String titreCategorie = "Menu";
		
		titre = new Text();
		
		titre.setText(titreCategorie);				
		titre.setFont(Font.loadFont(cheminRessources,tailleTextTitre));
		titre.setTextAlignment(TextAlignment.JUSTIFY);
		
	    bground = new Rectangle();

		FlowPane t= new FlowPane();
		t.getChildren().addAll(Placement,equipement,ventes,planning);
	    t.setHgap(100);
	    t.setVgap(50);
	    t.setPadding(new Insets(50));
	    t.setAlignment(Pos.BOTTOM_RIGHT);
		
		bground.setWidth(310);
		bground.setFill(Color.GREY);
		bground.setArcHeight(8);
		bground.setArcWidth(8);
	  
		
		
		
		vBoxGauche.setAlignment(Pos.CENTER);
		centreHbox.getChildren().addAll(vBoxGauche, vBoxDroite, t);

		BorderPane racine = new BorderPane();
		racine.setCenter(centreHbox);
		racine.setTop(topRect);
		racine.setBottom(botRect);
		
		return racine;	
	}
}
