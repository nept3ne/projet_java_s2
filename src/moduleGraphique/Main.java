package moduleGraphique;


import javafx.application.*;
import javafx.stage.*;
import moduleGestion.Festival;
import moduleGestion.Sauvegarde;

public class Main extends Application{
	private static Festival  fest = new Festival(" test ", " test ");
	public static String nomFest;
	public static String villeFest;
	public static String nomFichier;
	private static FenMenu fenMenu = new FenMenu(); 
	private static FenAcc fenAcc = new FenAcc();
	private static FenPlacement fenPlacement = new FenPlacement();
	private static Sauvegarde savegarde = new Sauvegarde();
	private static fenetreAjoutElement fAjElem = new fenetreAjoutElement();
	private static FenetrePlanning fpla = new FenetrePlanning();

	public void start(Stage primaryStage) {
		primaryStage = fenAcc;
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		
		Application.launch();
	}
	public static void ouvrirFenMenu() {
//		if (index==1) {	
//			savegarde.restoreFestival(nomFichier);
//			
//			if (fest!=null) {
//				nomFest=Festival.getNom();
//				villeFest=Festival.getVille();
//				fenAcc.close();
//				fenMenu.show();
//				System.out.println(fest +"  file dyserialized  ");
//			}
//		}else {
//			
//			// sauvegarde
//			fest = new Festival(nomFest, villeFest);
//			savegarde.sauverFestival(nomFichier);
//			//savegarde.restoreFestival(nomFichier);
//			System.out.println(" fest " + Festival.getNom());
//			
//			fenMenu.show();
//			fenAcc.close();
//		}		
		//code en rapport avec la serialisation
		fenMenu.show();
		fenAcc.close();
		fenPlacement.close();
		
	}
	public void test() {
	}
	
	public static Festival getFestival() {
		return fest;
	}
	public static void setFestival(Festival festival) {
		fest = festival;
	}
	// attention faire dans la classe sauvegarde, qui va faire une instance de festival a partir du festvial qui est en static dans cette class
		
	public void fermeture() throws Exception {
		savegarde.restoreFestival(nomFichier);
	}
	public static void ouvrirPlacement() {
		fenMenu.close();
		fenPlacement.show();
	}
	public static void ouvrirFenetreAjoutElement() {
		fAjElem.show();
	}
	public static void afficherPlanning() {
		fpla.show();
	}
	

}