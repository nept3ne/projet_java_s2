package moduleGraphique;

import java.io.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;

public class FenAcc extends Stage {
	
//	static private FenMenu menu = new FenMenu();
//	static private FenetrePlanning pla = new FenetrePlanning();
//	static private FenPlacement placer = new FenPlacement();
//	static private FenCompta_Plusinfo plus = new FenCompta_Plusinfo();
	

	//variable pour le fileChooser
	private final FileChooser fileChooser = new FileChooser();
	//bouton pour activer le file chooser
	private Button btnFile = new Button("Ouvrir un fichier");
	//formes pour la partie superieur du borderPane
	private Rectangle rectTop1 = new Rectangle();
	private Rectangle rectTop2 = new Rectangle();
	private Rectangle rectTop3 = new Rectangle();
	private Text txtTop3 = new Text();
	private int tailleTxtTop3 = 20;

	// formes pour la partie inferieur du borderPane 
	private Rectangle rectBot1 = new Rectangle();
	private Rectangle rectBot2 = new Rectangle();
	private Rectangle rectBot3 = new Rectangle();
	private Button butBot3 = new Button("Quitter");

	//formes pour la partie centrale du border
	//couleur de fond
	//StackPane 1
	private Rectangle rectCent1 = new Rectangle();
	private TextField txtFieldCent1 = new TextField();

	private Text txtCent1 = new Text("Entrez le nom du festival");	
	//StackPane 2
	private Rectangle rectCent2 = new Rectangle();
	private TextField txtFieldCent2 = new TextField();
	private Text txtCent2 = new Text("Entrez la ville du festival");
	//StackPane 3 
	private Rectangle rectCent3 = new Rectangle();
	private Button butCent3 = new Button("Ouvrir");
	//texte du panneau central
	private Text textBot2My  = new Text("Maryne G");
	private Text textBot2Ma = new Text("Matthis G");
	private Text textBot2Ql = new Text("Quentin L");
	private Text textBot2Cd = new Text("Corentin DR");

	//autres variables
	private int widthFenPrin=1000; //defini la largeur de la fentre principal
	private int heightFenPrin=600; // defini la hauteur de la fenetre principal
	private boolean tailleChangeable = false; // autorise le changement de la taille de la fenetre par l'utilisateur
	private int hauteurArrondiCent = 10; //defini la hauteur de l'arrondis des rectangles centraux
	private int largeurArrondiCent = 10; // defini la largeur de l'arrondis des rectangles centraux
	//Autres constantes
	private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
	private final int largeurRectangleSup=300; // defini la largeur de chaque rectangles superieurs
	private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
	private final int largeurRectangleInf=300;// defini la largeur de chaque rectangles inferieur
	private final int largStrokCent=4; // defini l'epaisseur des bordures des rectangles centraux
	private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
	private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
	private final int spacingCent=70; // espace entre les objets centraux
	private final Insets paddingCent= new Insets(20); // espaces entre les objets centraux et le contours
	private final int centWidth=900; // largeur de la partie centrale de la fenetre
	private final int centHeight=1000; // hauteur de la partie centrale de la fenetre
	private final int largeurTxtWidthCent=220; // largeur des blocs textes dans la fenetre centrale
	private final int largeurRectCent=100; // largeur des rectangles centraux 1 et 2 eme rectangle
	private final int hauteurRectCent=300; // hauteur des rectangles centraux 1 et 2 eme rectangle
	private final int largeurRectCent3=100; // largeur des rectangles centraux 1 et 2 eme rectangle
	private final int hauteurRectCent3=500; // hauteur des rectangles centraux 1 et 2 eme rectangle
	private final int tailleTxtButCent3 = 20; // taille du texte du bouton central
	private final int tailleTxtCent = 20; // taille du texte central (hors bouton et textField)
	private final int tailleTxtFieldCent = 15; // taille du text des zones textField
	//Variables d initialisation de la fenï¿½tre + bord superieur et inferieur
	private final int largeurRectangleBot_Cent = 500; // largeur du rectangle inferieur centre
	private final int hauteurRectangleBot_Cent = 50; // hauteur du rectangle inferieur centre
	private final int tailleTxtBotCent = 11; // taille du texte contenu le rectangle centrale inferieur
	private final int spacingRectCent3 = 15; // ajustement de l'espace entre les rectangles centraux du troisieme rang
	private final int hauteurBoutonBut3 = 30;
	private final int largeurBoutonBut3 = 100;

	private final String cheminRessources="file:ressources/fonts/Montserrat-Regular.ttf"; // defini le chemin vers la police Montserrat
	//on doit passer par la classe main, pour obtenir certaines methodes qui ne peuvent etre placee en static

	
	
	public FenAcc() {

		this.setTitle("Festiv'Org");
		this.setResizable(false);
		this.setX(200);
		this.setY(200);
		Scene SceneAcc = new Scene(accContenu());
		this.setScene(SceneAcc);
		this.setResizable(tailleChangeable);
		this.setWidth(widthFenPrin);
		this.setHeight(heightFenPrin);
		this.sizeToScene();
	}

	private Parent accContenu() {
		BorderPane racine = new BorderPane();
		Window racineFile =null;

		//---------------------------------------------------------------------------------------------------------------------------------------------------
		//------------------------------ Application des polices ------------------------------------------------------------------------------------
		txtCent1.setFont(Font.loadFont(cheminRessources, tailleTxtCent));
		txtCent2.setFont(Font.loadFont(cheminRessources, tailleTxtCent));
		txtFieldCent1.setFont(Font.loadFont(cheminRessources, tailleTxtFieldCent));
		txtFieldCent2.setFont(Font.loadFont(cheminRessources, tailleTxtFieldCent));
		butCent3.setFont(Font.loadFont(cheminRessources, tailleTxtButCent3));
		btnFile.setFont(Font.loadFont(cheminRessources, tailleTxtButCent3));
		txtTop3.setFont(Font.loadFont(cheminRessources, tailleTxtTop3));
		textBot2My.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
		textBot2Ma.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
		textBot2Ql.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
		textBot2Cd.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
		// --------------------------------------------------------------------------------------------------------------------------------------------------
		// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
		HBox topRect = new HBox();
		StackPane topStackPane = new StackPane();		

		rectTop1.setFill(Color.GREY);
		rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
		rectTop3.setFill(Color.GREY);


		rectTop1.setWidth(largeurRectangleSup);
		rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
		rectTop3.setWidth(largeurRectangleSup);

		rectTop1.setHeight(hauteurRectangleSup);
		rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
		rectTop3.setHeight(hauteurRectangleSup);

		rectTop1.setStroke(Color.BLACK);
		rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
		rectTop3.setStroke(Color.BLACK);

		rectTop1.setStrokeWidth(largStrokTop);
		rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
		rectTop3.setStrokeWidth(largStrokTop);

		txtTop3.setText("Festiv'Org"); // fixe le texte superieur
		topStackPane.getChildren().addAll(rectTop2, txtTop3);

		topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);

		//-----------------------------------------------------------------------------------------------------------------------------------------------------
		//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------

		HBox botRect1 = new HBox();

		FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
		nomPane.setAlignment(Pos.CENTER);
		nomPane.setColumnHalignment(HPos.CENTER);

		nomPane.setPrefWidth(largeurRectangleBot_Cent);
		nomPane.setPrefHeight(hauteurRectangleBot_Cent);
		nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);



		rectBot1.setFill(Color.GREY);
		rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
		rectBot3.setFill(Color.GREY);

		rectBot1.setWidth(largeurRectangleInf);
		rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
		rectBot3.setWidth(largeurRectangleInf);

		rectBot1.setHeight(hauteurRectangleInf);
		rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
		rectBot3.setHeight(hauteurRectangleInf);

		rectBot1.setStroke(Color.BLACK);
		rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
		rectBot3.setStroke(Color.BLACK);

		rectBot1.setStrokeWidth(largStrokeBot);
		rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
		rectBot3.setStrokeWidth(largStrokeBot);
		
		butBot3.setPrefWidth(largeurBoutonBut3);
		butBot3.setPrefHeight(hauteurBoutonBut3);

		StackPane btnStack = new StackPane();
		btnStack.setPrefWidth(hauteurRectangleInf);
		btnStack.setMaxWidth(largeurRectangleInf);
		btnStack.getChildren().addAll(rectBot3, butBot3);
		
		butBot3.setOnAction(e->{
			this.close();
		});
		
		StackPane nomStack = new StackPane();
		
		nomStack.setPrefWidth(largeurRectangleBot_Cent);
		nomStack.setMaxWidth(hauteurRectangleBot_Cent);
		nomStack.getChildren().addAll(rectBot2, nomPane);
		
		botRect1.getChildren().addAll(rectBot1,nomStack,btnStack);
		//----------------------------------------------------------Fin bord superieur et inferieur------------------------------------------------------------------
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		//------------------------------------------------------------------------------------------------------------------------------------------------------
		//--------------------------Ajustement des rectangles Centraux----------------------------------------------------------------------------------

		
		
		VBox cenRect = new VBox();

		VBox cenNomVbox = new VBox();
		StackPane cenNomStack = new StackPane();
		VBox cenVilleVbox = new VBox();
		StackPane cenVilleStack = new StackPane();
		StackPane cenBtnStack = new StackPane();

		//seulement l arriere plan du txt etait gris, il manquait les bordures en noir
		//txtTop3.setStyle("-fx-background-color : grey");

		rectCent1.setFill(Color.GREY);
		rectCent2.setFill(Color.GREY); // met la couleur grise des rectangle centraux
		rectCent3.setFill(Color.GREY);

		rectCent1.setWidth(hauteurRectCent);
		rectCent2.setWidth(hauteurRectCent);// 
		rectCent3.setWidth(hauteurRectCent3);

		rectCent1.setHeight(largeurRectCent);
		rectCent2.setHeight(largeurRectCent);
		rectCent3.setHeight(largeurRectCent3);

		rectCent1.setStroke(Color.BLACK);
		rectCent2.setStroke(Color.BLACK);
		rectCent3.setStroke(Color.BLACK);

		rectCent1.setStrokeWidth(largStrokCent);
		rectCent2.setStrokeWidth(largStrokCent);
		rectCent3.setStrokeWidth(largStrokCent);

		txtFieldCent1.setAlignment(Pos.CENTER);
		txtFieldCent2.setAlignment(Pos.CENTER);

		txtFieldCent1.setMaxWidth(largeurTxtWidthCent);
		txtFieldCent2.setMaxWidth(largeurTxtWidthCent);

		txtFieldCent1.setPrefWidth(largeurTxtWidthCent);
		txtFieldCent2.setPrefWidth(largeurTxtWidthCent);



		rectCent1.setArcHeight(hauteurArrondiCent);
		rectCent2.setArcHeight(hauteurArrondiCent);
		rectCent3.setArcHeight(hauteurArrondiCent);

		rectCent1.setArcWidth(largeurArrondiCent);
		rectCent2.setArcWidth(largeurArrondiCent);
		rectCent3.setArcWidth(largeurArrondiCent);

		HBox cenBtnHBox = new HBox();
		griserBouton();
		txtFieldCent1.setOnKeyPressed(e->griserBouton());
		txtFieldCent2.setOnKeyPressed(e->griserBouton());
		
		cenBtnHBox.getChildren().addAll(btnFile, butCent3);
		cenBtnHBox.setSpacing(spacingRectCent3);
		cenBtnHBox.setAlignment(Pos.CENTER);

		cenNomVbox.setAlignment(Pos.CENTER);
		cenVilleVbox.setAlignment(Pos.CENTER);

		cenNomVbox.getChildren().addAll(txtCent1,txtFieldCent1);
		cenVilleVbox.getChildren().addAll(txtCent2,txtFieldCent2);

		cenNomStack.getChildren().addAll(rectCent1, cenNomVbox);
		cenVilleStack.getChildren().addAll(rectCent2,cenVilleVbox);
		cenBtnStack.getChildren().addAll(rectCent3, cenBtnHBox);


		cenRect.setAlignment(Pos.CENTER_LEFT);
		cenRect.prefWidth(centWidth);
		cenRect.prefHeight(centHeight);
		cenRect.getChildren().addAll(cenNomStack, cenVilleStack, cenBtnStack);
		cenRect.setPadding(paddingCent);
		cenRect.setSpacing(spacingCent);

		racine.setTop(topRect);
		racine.setCenter(cenRect);
		racine.setBottom(botRect1);
	

		btnFile.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				// TODO Auto-generated method stub
				File file = fileChooser.showOpenDialog(racineFile);
				if (file!=null ) {		
					Main.nomFichier=file.getPath();
					System.out.println(Main.nomFichier);
					try {
						Main.ouvrirFenMenu();
				}catch(Exception e) { System.out.println(" error ");}
				}
			}

		});
		butCent3.setOnAction(e->{
			String i1 = txtFieldCent1.getText();
			String i2 = txtFieldCent2.getText();
			String chemin = i1+i2+"Festival.json";
			Main.nomFest=i1;
			Main.villeFest=i2;
			Main.nomFichier=chemin;
			try {
				Main.ouvrirFenMenu();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		});
		return racine;
	}
	
//	public void afficheMenu() {
//		menu.show();
//	}
//	
//	public void affichePanning() {
//		pla.show();
//	}
//	
//	public void affichePlacement() {
//		placer.show();
//	}
	
//	public void afficheplusinfo() {
//		plus.show();
//	}
	private void griserBouton() {
		if(txtFieldCent2.getText().isEmpty() || txtFieldCent1.getText().isEmpty()) {
			butCent3.setDisable(true);
		}else {
			butCent3.setDisable(false);
		}
	}

}
