package moduleGraphique;

import java.io.*;
import java.util.ArrayList;

import moduleGestion.Festival;
import moduleGestion.Prestation;

public class LectureFichierPlanning {
	String nomFichierEntree = "file:planning.csv"; 
	ArrayList<Prestation> liste;
	Prestation prest;
    BufferedReader entree;
    String ligne = new String();
    
    public LectureFichierPlanning() {
    	liste = new ArrayList<Prestation>();
    }
    
    public void lectureFic() throws FileNotFoundException{
    	BufferedReader entree;
    	
	    try {
	    	entree = new BufferedReader(new FileReader(nomFichierEntree));
			// boucle de lecture du fichier d'entrée ; lecture ligne par ligne
			for (ligne = entree.readLine(); ligne != null; ligne = entree.readLine()) {
				
				// la ligne courante est découpée en plusieurs éléments, copiés dans un tableau
				String elements[]=ligne.split(";");
				
				//boucle d'exploitation des différents éléments lus sur la ligne (exemple)
				for (int i=0 ; i<elements.length ; i++){
						prest = new Prestation(Integer.parseInt(elements[0]), Integer.parseInt(elements[2]), elements[3], Boolean.valueOf(elements[4]));
					}
				liste.add(prest);
				}
			entree.close();
		    } catch (FileNotFoundException e) {
		    	e.printStackTrace();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }       
    }  
    
    public ArrayList<Prestation> getliste(){
    	try {
			lectureFic();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return liste;
    }
    
}
