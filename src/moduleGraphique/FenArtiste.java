package moduleGraphique;

import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;

public class FenArtiste extends Stage {
	//formes pour la partie superieur du borderPane
		private Rectangle rectTop1 = new Rectangle();
		private Rectangle rectTop2 = new Rectangle();
		private Rectangle rectTop3 = new Rectangle();
		private Text txtTop3 = new Text();
		// formes pour la partie inferieur du borderPane 
		private Rectangle rectBot1 = new Rectangle();
		private Rectangle rectBot2 = new Rectangle();
		private Rectangle rectBot3 = new Rectangle();
		private Button butBot3 = new Button("Fermer");
		
		private TextField txtFieldCent1 = new TextField();
		private TextField txtFieldCent2 = new TextField();
		
		private Text textBot2My  = new Text("Maryne G");
		private Text textBot2Ma = new Text("Matthis G");
		private Text textBot2Ql = new Text("Quentin L");
		private Text textBot2Cd = new Text("Corentin DR");
		
		//Variable pour la partie centrale 
		private Text nom = new Text("Artiste");
		private Button valider = new Button("Valider");
		private Text nomA = new Text ("Nom de l'artiste :");
		private Text cachet = new Text ("Entrez le cachet :");
		
		//Rectangle pour la partie centrale
		private Rectangle rect1 = new Rectangle();
		private Rectangle rect2 = new Rectangle();
		
		//Variables d initialisation de la fenÃ¯Â¿Â½tre + bord superieur et inferieur
		private int widthFenPrin=500; //defini la largeur de la fentre principal
		private int heightFenPrin=300; // defini la hauteur de la fenetre principal
		private boolean tailleChangeable = false; // autorise le changement de la taille de la fenetre par l'utilisateur
		private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
		private final int largeurRectangleSup=200; // defini la largeur de chaque rectangles superieurs
		private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
		private final int largeurRectangleInf=200;// defini la largeur de chaque rectangles inferieur
		private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
		private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
		private final String cheminRessources="file:ressources/fonts/Montserrat-Regular.ttf"; // defini le chemin vers la police Montserrat
		private final int largeurRectangleBot_Cent = 200; // largeur du rectangle inferieur centre
		private final int hauteurRectangleBot_Cent = 70; // hauteur du rectangle inferieur centre
		private final int hauteurBoutonBut3 = 30;
		private final int largeurBoutonBut3 = 60;
		private final int tailleTxtBotCent = 20;
		private final int tailleTxtBotCent2 = 11;
		private final int centWidth=900; // largeur de la partie centrale de la fenetre
		private final int centHeight=1000; // hauteur de la partie centrale de la fenetre
		private final int largeurRectCent=50; // largeur des rectangles centraux 1 et 2 eme rectangle
		private final int hauteurRectCent=170; // hauteur des rectangles centraux 1 et 2 eme rectangle
		private final int largStrokCent=4; // defini l'epaisseur des bordures des rectangles centraux
		private final int spacingCent=50; // espace entre les objets centraux
		private final Insets paddingCent= new Insets(10); // espaces entre les objets centraux et le contours
		private final int largeurTxtWidthCent=150; // largeur des blocs textes dans la fenetre centrale
		
		// elements de la zone principal de la fenetre :
		// Variables dedie images : flecheRetour
		private final String cheminFlecheRetour = "file:ressources/Images/flecheRetour.png";
		private int flecheRetourWidth = 50;
		private int flecheRetourHeight = 100;
		private boolean flecheRetourRatio = true;
		private boolean flecheRetourSmooth = true;
		Image flecheRetour ;
		// autres de la zone principal
		private int tailleTextTitre=25;
	
	public FenArtiste() {

		this.setTitle("Festiv'Org");
		this.setResizable(false);
		Scene SceneAcc = new Scene(accContenu());
		this.setScene(SceneAcc);
		this.setResizable(tailleChangeable);
		this.setWidth(widthFenPrin);
		this.setHeight(heightFenPrin);
		this.sizeToScene();
	}

	private Parent accContenu() {
		// --------------------------------------------------------------------------------------------------------------------------------------------------
		// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
		HBox topRect = new HBox();
		StackPane topStackPane = new StackPane();		
		
		rectTop1.setFill(Color.GREY);
		rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
		rectTop3.setFill(Color.GREY);
		
		
		rectTop1.setWidth(largeurRectangleSup);
		rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
		rectTop3.setWidth(largeurRectangleSup);
		
		rectTop1.setHeight(hauteurRectangleSup);
		rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
		rectTop3.setHeight(hauteurRectangleSup);
		
		rectTop1.setStroke(Color.BLACK);
		rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
		rectTop3.setStroke(Color.BLACK);
		
		rectTop1.setStrokeWidth(largStrokTop);
		rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
		rectTop3.setStrokeWidth(largStrokTop);

		txtTop3.setText("Festiv'Org"); // fixe le texte superieur
		txtTop3.setFont(Font.loadFont(cheminRessources,tailleTextTitre));
		topStackPane.getChildren().addAll(rectTop2, txtTop3);
		
		topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);
		
//-----------------------------------------------------------------------------------------------------------------------------------------------------
		//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------
		
		HBox botRect = new HBox();
		
		FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
		nomPane.setAlignment(Pos.CENTER);
		nomPane.setColumnHalignment(HPos.CENTER);
		
		nomPane.setPrefWidth(largeurRectangleBot_Cent);
		nomPane.setPrefHeight(hauteurRectangleBot_Cent);
		nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);
		
		
		
		rectBot1.setFill(Color.GREY);
		rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
		rectBot3.setFill(Color.GREY);

		rectBot1.setWidth(largeurRectangleInf);
		rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
		rectBot3.setWidth(largeurRectangleInf);
		
		rectBot1.setHeight(hauteurRectangleInf);
		rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
		rectBot3.setHeight(hauteurRectangleInf);
		
		rectBot1.setStroke(Color.BLACK);
		rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
		rectBot3.setStroke(Color.BLACK);
		
		rectBot1.setStrokeWidth(largStrokeBot);
		rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
		rectBot3.setStrokeWidth(largStrokeBot);
		
		butBot3.setPrefWidth(largeurBoutonBut3);
		butBot3.setPrefHeight(hauteurBoutonBut3);
		
		butBot3.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent2));
		
		
		
		StackPane btnStack = new StackPane();
		btnStack.setPrefWidth(hauteurRectangleInf);
		btnStack.setMaxWidth(largeurRectangleInf);
		btnStack.getChildren().addAll(rectBot3, butBot3);
		
		
		StackPane nomStack = new StackPane();
		
		nomStack.setPrefWidth(largeurRectangleBot_Cent);
		nomStack.setMaxWidth(hauteurRectangleBot_Cent);
		nomStack.getChildren().addAll(rectBot2, nomPane);
		
		botRect.getChildren().addAll(rectBot1,nomStack,btnStack);
//----------------------------------------------------------Fin bord superieur et inferieur------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		VBox cenRect = new VBox();
		HBox centreHbox = new HBox();
		VBox vBoxGauche = new VBox();
		GridPane placer = new GridPane();
		StackPane cenVilleStack = new StackPane();
		StackPane cenNomStack = new StackPane();
		
		flecheRetour = new Image(cheminFlecheRetour, flecheRetourWidth, flecheRetourHeight, flecheRetourRatio, flecheRetourSmooth);
		ImageView flecheRetourView = new ImageView(flecheRetour);
						
		nom.setFont(Font.loadFont(cheminRessources,tailleTextTitre));
		nom.setTextAlignment(TextAlignment.CENTER);
		nomA.setFont(Font.loadFont(cheminRessources,tailleTextTitre));
		nomA.setTextAlignment(TextAlignment.LEFT);
		cachet.setFont(Font.loadFont(cheminRessources,tailleTextTitre));
		cachet.setTextAlignment(TextAlignment.LEFT);
		
		valider.setFont(Font.loadFont(cheminRessources,tailleTxtBotCent));
		
		rect1.setFill(Color.GREY);
		rect2.setFill(Color.GREY); // met la couleur grise des rectangle centraux

		rect1.setWidth(hauteurRectCent);
		rect2.setWidth(hauteurRectCent);// 

		rect1.setHeight(largeurRectCent);
		rect2.setHeight(largeurRectCent);

		rect1.setStroke(Color.BLACK);
		rect2.setStroke(Color.BLACK);

		rect1.setStrokeWidth(largStrokCent);
		rect2.setStrokeWidth(largStrokCent);
		
		txtFieldCent1.setAlignment(Pos.CENTER);
		txtFieldCent2.setAlignment(Pos.CENTER);

		txtFieldCent1.setMaxWidth(largeurTxtWidthCent);
		txtFieldCent2.setMaxWidth(largeurTxtWidthCent);

		txtFieldCent1.setPrefWidth(largeurTxtWidthCent);
		txtFieldCent2.setPrefWidth(largeurTxtWidthCent);
		
		cenNomStack.getChildren().addAll(rect1,txtFieldCent1);
		cenVilleStack.getChildren().addAll(rect2,txtFieldCent2);
		
		placer.add(nomA, 0,0);
		placer.add(cenNomStack, 1, 0);
		placer.add(cenVilleStack, 1, 1);
		placer.add(cachet, 0, 1);
		placer.setVgap(50);
		
			
		cenRect.setAlignment(Pos.CENTER);
		cenRect.prefWidth(centWidth);
		cenRect.prefHeight(centHeight);
		cenRect.getChildren().addAll(nom,placer, valider);
		cenRect.setPadding(paddingCent);
		cenRect.setSpacing(spacingCent);
		
//		cenRect.setStyle("-fx-border-color: blue;\n"
//                + "-fx-border-insets: 5;\n"
//                + "-fx-border-width: 3;\n"
//                + "-fx-border-style: dashed;\n");
		
		vBoxGauche.getChildren().addAll(flecheRetourView);
		vBoxGauche.setAlignment(Pos.TOP_LEFT);
		
		centreHbox.getChildren().addAll(cenRect);
		centreHbox.setAlignment(Pos.TOP_CENTER);

		
		BorderPane racine = new BorderPane();
		racine.setTop(topRect);
		racine.setLeft(vBoxGauche);
		racine.setCenter(centreHbox);
		racine.setBottom(botRect);
		
		return racine;	
	}
}
