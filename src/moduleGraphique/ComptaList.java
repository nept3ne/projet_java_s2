//package moduleGraphique;
//
//import java.util.*;
//
//import javafx.geometry.*;
//import javafx.scene.control.*;
//import javafx.scene.layout.*;
//import javafx.scene.text.*;
//
//public class ComptaList {
//	
//	private ArrayList<HBox> listeHBox;
//	private ArrayList<Button> listeBouton;
//	private ArrayList<TextField> listeIntitule;
//	private ArrayList<TextField> listePrix;
//	private HashMap<String,Float> listeText;
//	
//	private final String cheminRessources="file:ressources/fonts/Montserrat-Regular.ttf"; // defini le chemin vers la police Montserrat
//	private int tailleTxtMoins = 15;
//	private int depensesMaxWidth=150;
//	private int saisePrixMaxWidth=70;
//
//	
//	ComptaList(){
//		listeHBox = new ArrayList<HBox>(100);
//		listeBouton = new ArrayList<Button>(100);
//		listeIntitule = new ArrayList<TextField>(100);
//		listePrix = new ArrayList<TextField>(100);
//		listeText = new HashMap<String, Float>();
//		
//}
//	public HBox newHBox(String chaine, String nom, String prix) {
//		try {
//			HBox localDepense = new HBox();
//			Button txt = new Button();
//			TextField depensesTxtField = new TextField();
//			TextField saisiePrix = new TextField();
//			
//			
//			txt.setText(chaine);
//			txt.setFont(Font.loadFont(cheminRessources, tailleTxtMoins));
//			
//			txt.minWidth(50);
//			txt.maxWidth(50);
//	
//			depensesTxtField.setText(nom);
//			depensesTxtField.setFont(Font.loadFont(cheminRessources,tailleTxtMoins));
//			depensesTxtField.setMaxWidth(depensesMaxWidth);
//			depensesTxtField.setPrefWidth(depensesMaxWidth);
//			System.out.println(depensesTxtField.getText());
//			
//			saisiePrix.setText(prix);
//			saisiePrix.setPrefWidth(saisePrixMaxWidth);
//			saisiePrix.setMaxWidth(saisePrixMaxWidth);
//			saisiePrix.setFont(Font.loadFont(cheminRessources, tailleTxtMoins));
//			
//			localDepense.setSpacing(10);
//			localDepense.setAlignment(Pos.CENTER);
//			localDepense.setPadding(new Insets(10,0,10,0));
//			
//			localDepense.getChildren().addAll(txt, depensesTxtField, saisiePrix);
//			
//			listeBouton.add(txt);
//			listeIntitule.add(depensesTxtField);
//			listePrix.add(saisiePrix);
//			try{
//				listeText.put(nom, Float.valueOf(prix));
//			}catch(Exception e) {
//				listeText.put(nom, (float) -1);
//				System.out.println(" erreur : " + e + " le string a �t� gard�, mais -1 a �t� plac� dans listeText");
//			}
//			
//			listeHBox.add(localDepense);
//			return localDepense;
//		}catch(Exception e) {
//			System.out.println(" exception newHBox :  " + e );
//			return null;
//		}
//	}
//	
//	
//	public Button getBoutonMoins(int index) {
//		return listeBouton.get(index);
//	}
//	public TextField getTextFieldIntitule(int index) {
//		return this.listeIntitule.get(index);
//	}
//	public TextField getTextFieldPrix(int index) {
//		return this.listePrix.get(index);
//	}
//	public String getIntitule(int index) {
//		Iterator<String> it = this.listeText.keySet().iterator();
//		String key=null;
//		int i=0;
//		boolean trouve=false;
//		while (it.hasNext() && !(trouve)) {
//			key = it.next();
//			if (i==index) {
//				trouve=true;
//			}
//			i++;
//		}
//		return key;
//	}
//	
//	public Float getPrix(int index) {
//		Iterator<String> it = this.listeText.keySet().iterator();
//		String key=null;
//		int i=0;
//		float ret=-1;
//		boolean trouve=false;
//		while (it.hasNext() && !(trouve)) {
//			key = it.next();
//			if (i==index) {
//				trouve=true;
//				ret=listeText.get(key);
//			}
//			i++;
//		}
//		
//		return ret;
//	}
//	
//	public HBox getHBox(int index) {
//		return listeHBox.get(index);
//	}
//	
//	public void setters(int index, char signe, String chaine, String prix) {
//		HBox localDepense = new HBox();
//		Button txt = new Button();
//		TextField depensesTxtField = new TextField();
//		TextField saisiePrix = new TextField();
//		
//		
//		txt.setText(String.valueOf(signe));
//		txt.setFont(Font.loadFont(cheminRessources, tailleTxtMoins));
//		
//		txt.minWidth(50 );
//		txt.maxWidth(50);
//
//		depensesTxtField.setText(chaine);
//		depensesTxtField.setFont(Font.loadFont(cheminRessources,tailleTxtMoins));
//		depensesTxtField.setMaxWidth(depensesMaxWidth);
//		depensesTxtField.setPrefWidth(depensesMaxWidth);
//		System.out.println(depensesTxtField.getText());
//		
//		saisiePrix.setText(String.valueOf(prix));
//		saisiePrix.setPrefWidth(saisePrixMaxWidth);
//		saisiePrix.setMaxWidth(saisePrixMaxWidth);
//		saisiePrix.setFont(Font.loadFont(cheminRessources, tailleTxtMoins));
//		
//		localDepense.setSpacing(10);
//		localDepense.setAlignment(Pos.CENTER);
//		localDepense.setPadding(new Insets(10,0,10,0));
//		
//		localDepense.getChildren().addAll(txt, depensesTxtField, saisiePrix);
//		
//		listeBouton.set(index, txt);
//		listeIntitule.set(index, depensesTxtField);
//		listePrix.set(index, saisiePrix);
//		try{
//			listeText.replace(chaine, Float.valueOf(prix));
//		}catch(Exception e) {
//			localDepense=null;
//			System.out.println(" erreur : " + e + " attention la valeur retournée a ete mise a null par mesure de securite");
//		}
//		
//		listeHBox.add(localDepense);
//	}
//	
//	
//	
//}
