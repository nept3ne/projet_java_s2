//package moduleGraphique;
//
//import javafx.geometry.*;
//import javafx.scene.*;
//import javafx.scene.control.*;
//import javafx.scene.image.*;
//import javafx.scene.layout.*;
//import javafx.scene.paint.*;
//import javafx.scene.shape.*;
//import javafx.scene.text.*;
//import javafx.stage.*;
//
//public class FenCompta_Plusinfo extends Stage{
//	
//	//formes pour la partie superieur du borderPane
//	private Rectangle rectTop1 = new Rectangle();
//	private Rectangle rectTop2 = new Rectangle();
//	private Rectangle rectTop3 = new Rectangle();
//	private Text txtTop3 = new Text();
//	private int tailleTxtTop3 = 20;
//	// formes pour la partie inferieur du borderPane 
//	private Rectangle rectBot1 = new Rectangle();
//	private Rectangle rectBot2 = new Rectangle();
//	private Rectangle rectBot3 = new Rectangle();
//	private Button butBot3 = new Button("Fermer");
//	
//	private Text textBot2My  = new Text("Maryne G");
//	private Text textBot2Ma = new Text("Matthis G");
//	private Text textBot2Ql = new Text("Quentin L");
//	private Text textBot2Cd = new Text("Corentin DR");
//	
//	
//	
//	
//	//Variables d initialisation de la fenï¿½tre + bord superieur et inferieur
//	private int widthFenPrin=2000; //defini la largeur de la fentre principal
//	private int heightFenPrin=1000; // defini la hauteur de la fenetre principal
//	private boolean tailleChangeable = false; // autorise le changement de la taille de la fenetre par l'utilisateur
//	private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
//	private final int largeurRectangleSup=500; // defini la largeur de chaque rectangles superieurs
//	private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
//	private final int largeurRectangleInf=500;// defini la largeur de chaque rectangles inferieur
//	private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
//	private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
//	private final String cheminRessources="file:ressources/fonts/Montserrat-Regular.ttf"; // defini le chemin vers la police Montserrat
//	private final int largeurRectangleBot_Cent = 500; // largeur du rectangle inferieur centre
//	private final int hauteurRectangleBot_Cent = 70; // hauteur du rectangle inferieur centre
//	private final int tailleTxtBotCent = 11; // taille du texte contenu le rectangle centrale inferieur
//	private final int hauteurBoutonBut3 = 30;
//	private final int largeurBoutonBut3 = 300;
//	private final int tailleButBot3 = 20;
//	
//	// elements de la zone principal de la fenetre :
//	// Variables dedie images : flecheRetour
//	private final String cheminFlecheRetour = "file:ressources/Images/flecheRetour.png";
//	private int flecheRetourWidth = 50;
//	private int flecheRetourHeight = 100;
//	private boolean flecheRetourRatio = true;
//	private boolean flecheRetourSmooth = true;
//	Image flecheRetour ;
//	// autres de la zone principal
//	private Rectangle bground;
//	private Text titre ;
//	private Rectangle traitRect;
//	private Button moinsTxt;
//	private Button plus;
//	private TextField depensesTxtField;
//	private TextField saisiePrix;
//	private Button btnValider;
//	private Text resultat;
//	private final String largeurStroke = "5";
//	private final String styleDepense = "-fx-bacground-color : GREY;-fx-border-width : "+largeurStroke +"; -fx-border-color : BLACK;";
//	private int tailleTxtMoins = 15;
//	private int widthSaisiePrix = 20;
//	private HBox entreDepenseLigne;
//	private StackPane depenseStack;
//	private VBox entreDepensesConteneur ;
//	private int moinsMaxWidth=50;
//	
//	private int traitWidth=290;
//	private int traitHeight=2;
//	private int traitStrokeWidth=2;
//	private int traitArcHeight=2;
//	private int traitArcWidth=2;
//
//	private int plusMaxWidth=50;
//	private String styleBorderRadiusDepense="-fx-border-radius:8;";
//	private int resultatTotal=1000;
//	private int tailleTxtResultat=20;
//	private int entreDepensesHeight;
//	private TextField depense2, saisiePrix2;
//	private int tailleTextTitre=25;
//	private ComptaList cL ; //Objet qui va permetre de stocker et sauvegarder les valeurs de type l'intitule d'une depense ainsi que son montant
//	
//	// gestion de liste de depenses
//	
//	
//	
//	public FenCompta_Plusinfo() {
//			
//			this.cL = new ComptaList();
//			this.setTitle("Festiv'Org");
//			this.setResizable(false);
//			Scene SceneAcc = new Scene(accContenu());
//			this.setScene(SceneAcc);
//			this.setResizable(tailleChangeable);
//			this.setWidth(widthFenPrin);
//			this.setHeight(heightFenPrin);
//			this.sizeToScene();
//			
//			
//			System.out.println(cL);
//	}
//	
//	private Parent accContenu(){
//		
////-----------------------------------------------------------------------------------------------------------------------------------------------------------
////------------------------------------------------------ Bord Inferieur et Superieur de la fenetre-----------------------------------------------------------
//		//---------------------------------------------------------------------------------------------------------------------------------------------------
//				//------------------------------ Application des polices ------------------------------------------------------------------------------------
//				txtTop3.setFont(Font.loadFont(cheminRessources, tailleTxtTop3));
//				textBot2My.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
//				textBot2Ma.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
//				textBot2Ql.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
//				textBot2Cd.setFont(Font.loadFont(cheminRessources, tailleTxtBotCent));
//				butBot3.setFont(Font.loadFont(cheminRessources, tailleButBot3));
//		// --------------------------------------------------------------------------------------------------------------------------------------------------
//				// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
//				HBox topRect = new HBox();
//				StackPane topStackPane = new StackPane();		
//				
//				rectTop1.setFill(Color.GREY);
//				rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
//				rectTop3.setFill(Color.GREY);
//				
//				
//				rectTop1.setWidth(largeurRectangleSup);
//				rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
//				rectTop3.setWidth(largeurRectangleSup);
//				
//				rectTop1.setHeight(hauteurRectangleSup);
//				rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
//				rectTop3.setHeight(hauteurRectangleSup);
//				
//				rectTop1.setStroke(Color.BLACK);
//				rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
//				rectTop3.setStroke(Color.BLACK);
//				
//				rectTop1.setStrokeWidth(largStrokTop);
//				rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
//				rectTop3.setStrokeWidth(largStrokTop);
//
//				txtTop3.setText("Festiv'Org"); // fixe le texte superieur
//				topStackPane.getChildren().addAll(rectTop2, txtTop3);
//				
//				topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);
//				
//		//-----------------------------------------------------------------------------------------------------------------------------------------------------
//				//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------
//				
//				HBox botRect = new HBox();
//				
//				FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
//				nomPane.setAlignment(Pos.CENTER);
//				nomPane.setColumnHalignment(HPos.CENTER);
//				
//				nomPane.setPrefWidth(largeurRectangleBot_Cent);
//				nomPane.setPrefHeight(hauteurRectangleBot_Cent);
//				nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);
//				
//				
//				
//				rectBot1.setFill(Color.GREY);
//				rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
//				rectBot3.setFill(Color.GREY);
//
//				rectBot1.setWidth(largeurRectangleInf);
//				rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
//				rectBot3.setWidth(largeurRectangleInf);
//				
//				rectBot1.setHeight(hauteurRectangleInf);
//				rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
//				rectBot3.setHeight(hauteurRectangleInf);
//				
//				rectBot1.setStroke(Color.BLACK);
//				rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
//				rectBot3.setStroke(Color.BLACK);
//				
//				rectBot1.setStrokeWidth(largStrokeBot);
//				rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
//				rectBot3.setStrokeWidth(largStrokeBot);
//				
//				butBot3.setPrefWidth(largeurBoutonBut3);
//				butBot3.setPrefHeight(hauteurBoutonBut3);
//				
//				
//				
//				StackPane btnStack = new StackPane();
//				btnStack.setPrefWidth(hauteurRectangleInf);
//				btnStack.setMaxWidth(largeurRectangleInf);
//				btnStack.getChildren().addAll(rectBot3, butBot3);
//				
//				
//				StackPane nomStack = new StackPane();
//				
//				nomStack.setPrefWidth(largeurRectangleBot_Cent);
//				nomStack.setMaxWidth(hauteurRectangleBot_Cent);
//				nomStack.getChildren().addAll(rectBot2, nomPane);
//				
//				botRect.getChildren().addAll(rectBot1,nomStack,btnStack);
////----------------------------------------------------------Fin bord superieur et inferieur------------------------------------------------------------------
////-----------------------------------------------------------------------------------------------------------------------------------------------------------
//				
//				
////---------------------------------------------------------Fenetre principal --------------------------------------------------------------------------------
//				HBox centreHbox = new HBox();
//				
//				
//				VBox vBoxGauche = new VBox();
//					flecheRetour = new Image(cheminFlecheRetour, flecheRetourWidth, flecheRetourHeight, flecheRetourRatio, flecheRetourSmooth);
//					ImageView flecheRetourView = new ImageView(flecheRetour);
//					depenseStack = new StackPane();
//						bground = new Rectangle();
//						entreDepensesConteneur = new VBox();
//							titre = new Text();
//							traitRect = new Rectangle();
//							//HashSet de depense
//							entreDepenseLigne = new HBox();
//								plus = new Button();
//								depensesTxtField = new TextField();
//								saisiePrix = new TextField();
//							btnValider= new Button();
//					resultat = new Text();
//				VBox vBoxDroite = new VBox();				
//				
//				String titreCategorie = "Depense";
//
//				titre.setText(titreCategorie);				
//				titre.setFont(Font.loadFont(cheminRessources,tailleTextTitre));
//				titre.setTextAlignment(TextAlignment.JUSTIFY);
//				
//				
//				
//				traitRect.setWidth(traitWidth);
//				traitRect.setHeight(traitHeight);
//				traitRect.setStrokeWidth(traitStrokeWidth);
//				traitRect.setStroke(Color.BLACK);
//				traitRect.setArcHeight(traitArcHeight);
//				traitRect.setArcWidth(traitArcWidth);
//				//methode d'ajout du HashMap
//				
//				
//				
//				entreDepenseLigne.setSpacing(10);
//				entreDepenseLigne.setAlignment(Pos.CENTER);
//				entreDepenseLigne.setPadding(new Insets(10,0,10,0));
//				
//				btnValider.setText("Valider");			
//				btnValider.setFont(Font.loadFont(cheminRessources,tailleTxtMoins));
//				btnValider.setPrefWidth(290);
//				btnValider.setMaxWidth(290);
//				
//				bground.setWidth(310);
//				bground.setFill(Color.GREY);
//				bground.setArcHeight(8);
//				bground.setArcWidth(8);
//				
//				entreDepenseLigne.getChildren().addAll(plus, depensesTxtField, saisiePrix);
//				
//				entreDepensesConteneur.getChildren().addAll(titre, traitRect );
//				entreDepensesConteneur.setAlignment(Pos.CENTER);
//				//Ajout des depensesTxtField
//				try{
//					
//					entreDepensesConteneur.getChildren().add(cL.newHBox("-", "depense1", "1000"));
//				}catch(Exception e){
//					System.out.println( "exception ajout : " + e );
//				}
//				
//				System.out.println("test  :  " + cL.getPrix(1) + "  " + cL.getIntitule(1));
//				entreDepensesConteneur.getChildren().add(cL.newHBox("+", "entrez depense", "?"));
//				entreDepensesConteneur.getChildren().add(btnValider);
//				//---------------------------------------------------ATTENTION-------------------------------
//				//Ajouter une methode qui calcule/actualise la hauteur de "entreDepenses" a chaque fois qu'une categorie est modifie
//				entreDepensesHeight=200;
//				bground.setHeight(entreDepensesHeight);
//				System.out.println(entreDepensesConteneur.getHeight());
//				
//				StackPane.setAlignment(entreDepensesConteneur, Pos.CENTER);
//				StackPane.setAlignment(bground, Pos.CENTER);
//				
//				depenseStack.setPadding(new Insets(20,20,20,20));
//				
//				depenseStack.getChildren().addAll(bground, entreDepensesConteneur);
//				resultat.setText("Resultat : " + String.valueOf(resultatTotal));
//				resultat.setFont(Font.loadFont(cheminRessources,tailleTxtResultat));
//				
//				vBoxGauche.getChildren().addAll(flecheRetourView, depenseStack, resultat);
//				vBoxGauche.setAlignment(Pos.CENTER);
//				centreHbox.getChildren().addAll(vBoxGauche, vBoxDroite);
//				BorderPane racine = new BorderPane();
//				racine.setCenter(centreHbox);
//				racine.setTop(topRect);
//				racine.setBottom(botRect);
//				System.out.println(saisiePrix.getWidth());
//
//				return racine;	
//	}
//	public void setEntreDepensesConteneur(HBox hbox) {
//		this.entreDepensesConteneur.getChildren().add(hbox);
//	}
//}
