package moduleGraphique;



import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;
import moduleGestion.Placement;


public class FenPlacement extends Stage {
	//formes pour la partie superieur du borderPane
		private Rectangle rectTop1 = new Rectangle();
		private Rectangle rectTop2 = new Rectangle();
		private Rectangle rectTop3 = new Rectangle();
		private Text txtTop3 = new Text();
		// formes pour la partie inferieur du borderPane 
		private Rectangle rectBot1 = new Rectangle();
		private Rectangle rectBot2 = new Rectangle();
		private Rectangle rectBot3 = new Rectangle();
		private Button butBot3 = new Button("Fermer");
		
		private Text textBot2My  = new Text("Maryne G");
		private Text textBot2Ma = new Text("Matthis G");
		private Text textBot2Ql = new Text("Quentin L");
		private Text textBot2Cd = new Text("Corentin DR");
		private static AnchorPane aPCentral ;
		private static Rectangle rectCentral ;
		
		private static int choix=33;
		
		
		//Variables d initialisation de la fenï¿½tre + bord superieur et inferieur
		private int widthFenPrin=2000; //defini la largeur de la fentre principal
		private int heightFenPrin=1000; // defini la hauteur de la fenetre principal
		private boolean tailleChangeable = false; // autorise le changement de la taille de la fenetre par l'utilisateur
		private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
		private final int largeurRectangleSup=500; // defini la largeur de chaque rectangles superieurs
		private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
		private final int largeurRectangleInf=500;// defini la largeur de chaque rectangles inferieur
		private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
		private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
//		private final String cheminRessources="file:ressources/fonts/Montserrat-Regular.ttf"; // defini le chemin vers la police Montserrat
		private final int largeurRectangleBot_Cent = 500; // largeur du rectangle inferieur centre
		private final int hauteurRectangleBot_Cent = 70; // hauteur du rectangle inferieur centre
		private final int hauteurBoutonBut3 = 30;
		private final int largeurBoutonBut3 = 300;
		
		// elements de la zone principal de la fenetre :
		// Variables dedie images : flecheRetour
		private int flecheRetourWidth = 50;
		
		
		
		
		private static ObservableList<String> liste1 = FXCollections.observableArrayList();
		private static ListView<String> liste1View = new ListView<String>(liste1);

		// autres de la zone principal
		private static Placement place = Main.getFestival().getPlacement();
		private  static StackPane sPCentral;
	
	public FenPlacement() {

		this.setTitle("Festiv'Org");
		this.setResizable(false);
		this.setX(200);
		this.setY(200);
		Scene SceneAcc = new Scene(accContenu());
		this.setScene(SceneAcc);
		this.setResizable(tailleChangeable);
		this.setWidth(widthFenPrin);
		this.setHeight(heightFenPrin);
		this.sizeToScene();
//		SceneAcc.getStylesheets().add(this.getClass().getResource("CssScene.css").toExternalForm());
	}

	private Parent accContenu() {
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		// --------------------------------------------------------------------------------------------------------------------------------------------------
		BorderPane racine = new BorderPane();
		// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
		HBox topRect = new HBox();
		StackPane topStackPane = new StackPane();		
		
		rectTop1.setFill(Color.GREY);
		rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
		rectTop3.setFill(Color.GREY);
		
		
		rectTop1.setWidth(largeurRectangleSup);
		rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
		rectTop3.setWidth(largeurRectangleSup);
		
		rectTop1.setHeight(hauteurRectangleSup);
		rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
		rectTop3.setHeight(hauteurRectangleSup);
		
		rectTop1.setStroke(Color.BLACK);
		rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
		rectTop3.setStroke(Color.BLACK);
		
		rectTop1.setStrokeWidth(largStrokTop);
		rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
		rectTop3.setStrokeWidth(largStrokTop);

		txtTop3.setText("Festiv'Org"); // fixe le texte superieur
		topStackPane.getChildren().addAll(rectTop2, txtTop3);
		
		topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);
		
//-----------------------------------------------------------------------------------------------------------------------------------------------------
		//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------
		
		HBox botRect = new HBox();
		
		FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
		nomPane.setAlignment(Pos.CENTER);
		nomPane.setColumnHalignment(HPos.CENTER);
		
		nomPane.setPrefWidth(largeurRectangleBot_Cent);
		nomPane.setPrefHeight(hauteurRectangleBot_Cent);
		nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);
		
		
		
		rectBot1.setFill(Color.GREY);
		rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
		rectBot3.setFill(Color.GREY);

		rectBot1.setWidth(largeurRectangleInf);
		rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
		rectBot3.setWidth(largeurRectangleInf);
		
		rectBot1.setHeight(hauteurRectangleInf);
		rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
		rectBot3.setHeight(hauteurRectangleInf);
		
		rectBot1.setStroke(Color.BLACK);
		rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
		rectBot3.setStroke(Color.BLACK);
		
		rectBot1.setStrokeWidth(largStrokeBot);
		rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
		rectBot3.setStrokeWidth(largStrokeBot);
		
		butBot3.setPrefWidth(largeurBoutonBut3);
		butBot3.setPrefHeight(hauteurBoutonBut3);
		
		butBot3.setOnAction(e->this.close());
		
		StackPane btnStack = new StackPane();
		btnStack.setPrefWidth(hauteurRectangleInf);
		btnStack.setMaxWidth(largeurRectangleInf);
		btnStack.getChildren().addAll(rectBot3, butBot3);
		
		
		StackPane nomStack = new StackPane();
		
		nomStack.setPrefWidth(largeurRectangleBot_Cent);
		nomStack.setMaxWidth(hauteurRectangleBot_Cent);
		nomStack.getChildren().addAll(rectBot2, nomPane);
		
		botRect.getChildren().addAll(rectBot1,nomStack,btnStack);
		racine.setTop(topRect);
		racine.setBottom(botRect);
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------Fin bord superieur et inferieur------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		AnchorPane centreAp = new AnchorPane();
		VBox VBoxGauche = new VBox();
		VBox VBoxDroite = new VBox();
		HBox hBoxImgTxt = new HBox();
		
		Label Titre = new Label(" Gestion placement ");
				
		Button btnRetour = new Button("<");
		btnRetour.setPrefSize(flecheRetourWidth, flecheRetourWidth);
		btnRetour.setOnAction(e->Main.ouvrirFenMenu());
		
		
		
		
		
		hBoxImgTxt.getChildren().addAll(btnRetour, Titre);
		hBoxImgTxt.setSpacing(30);
		hBoxImgTxt.setAlignment(Pos.CENTER);
		
		
		sPCentral = new StackPane();
		sPCentral.setPrefHeight(400);
		sPCentral.setPrefWidth(400);
		
		rectCentral = new Rectangle();
		rectCentral.setWidth(400);
		rectCentral.setHeight(400);
		rectCentral.setFill(Color.WHITE);
		rectCentral.setStroke(Color.BLACK);
		rectCentral.setStrokeWidth(3);
		
		aPCentral = new AnchorPane();
		
		Image fBasCentral = new Image("flechebas.png", 20,20,true,true);
		Image fHautCentral = new Image("flechehaut.png", 20,20,true,true);
		Image fGaucheCentral = new Image("flechegauche.png", 20,20,true,true);
		Image fDroiteCentral = new Image("flechedroite.png", 20,20,true,true);
		
		ImageView fBasCentralView = new ImageView(fBasCentral);
		ImageView fHautCentralView = new ImageView(fHautCentral);
		ImageView fGaucheCentralView = new ImageView(fGaucheCentral);
		ImageView fDroiteCentralView = new ImageView(fDroiteCentral);
		
		Button basCentral = new Button("  ");
		Button hautCentral = new Button("  ");
		Button gaucheCentral = new Button("  ");
		Button droiteCentral = new Button("  ");
		
		
		basCentral.setOpacity(0.1);
		hautCentral.setOpacity(0.1);
		gaucheCentral.setOpacity(100);
		droiteCentral.setOpacity(0.1);
		
		basCentral.setPrefSize(20, 20);
		hautCentral.setPrefSize(20, 20);
		gaucheCentral.setPrefSize(20, 20);
		droiteCentral.setPrefSize(20, 20);
				
		fBasCentralView.setOnMouseClicked(e->{ // devrait marcher mais probleme au declenchement
				int index = liste1View.getSelectionModel().getSelectedIndex();
				if (index!=-1) {
					moduleGestion.Scene sc;
					int i;
					sc = place.getArrayScene().get(index);
					i=sc.getLongitude();
					if (i>-1) {
						sc.setLongitude(i+20);
						ajouterImageScene();
					}
					else {
						Alert a =new Alert(AlertType.ERROR, " vous avez atteint le max");
						a.show();
					}
				}});
		fHautCentralView.setOnMouseClicked(e->{ // devrait marcher mais probleme au declenchement
			int index = liste1View.getSelectionModel().getSelectedIndex();
			if (index!=-1) {
				moduleGestion.Scene sc;
				int i;
				sc = place.getArrayScene().get(index);
				i=sc.getLongitude();
				if (i<20) {
					sc.setLongitude(i+20);
					ajouterImageScene();
				}
				else {
					Alert a =new Alert(AlertType.ERROR, " vous avez atteint le max");
					a.show();
				}
			}});
		fDroiteCentralView.setOnMouseClicked(e->{ // devrait marcher mais probleme au declenchement
			int index = liste1View.getSelectionModel().getSelectedIndex();
			if (index!=-1) {
				moduleGestion.Scene sc;
				int i;
				sc = place.getArrayScene().get(index);
				i=sc.getLatitude();
				if (i<20) {
					sc.setLatitude(i+20);
					ajouterImageScene();
				}
				else {
					Alert a =new Alert(AlertType.ERROR, " vous avez atteint le max");
					a.show();
				}
			}});
		fGaucheCentralView.setOnMouseClicked(e->{ // devrait marcher mais probleme au declenchement
			int index = liste1View.getSelectionModel().getSelectedIndex();
			if (index!=-1) {
				moduleGestion.Scene sc;
				int i;
				sc = place.getArrayScene().get(index);
				i=sc.getLatitude();
				if (i>0) {
					sc.setLatitude(i+20);
					ajouterImageScene();
				}
				else {
					Alert a =new Alert(AlertType.ERROR, " vous avez atteint le max");
					a.show();
				}
			}});
		
		AnchorPane.setTopAnchor(fGaucheCentralView, (double) 25);
		AnchorPane.setTopAnchor(fDroiteCentralView, (double) 25);
		AnchorPane.setTopAnchor(fHautCentralView, (double) 5);
		AnchorPane.setTopAnchor(fBasCentralView, (double) 45);
		AnchorPane.setRightAnchor(fDroiteCentralView, (double) 45);
		AnchorPane.setRightAnchor(fGaucheCentralView, (double) 5);
		AnchorPane.setRightAnchor(fHautCentralView, (double) 25);
		AnchorPane.setRightAnchor(fBasCentralView, (double) 25);
		
		aPCentral.getChildren().addAll(fBasCentralView, fHautCentralView, fDroiteCentralView, fGaucheCentralView);
		
		sPCentral.getChildren().addAll(rectCentral,aPCentral);
		
		VBoxGauche.getChildren().addAll(hBoxImgTxt, sPCentral);
		VBoxGauche.setSpacing(10);
		VBoxGauche.getChildren().addAll();
		VBoxGauche.setPadding(new Insets(10, 0, 10, 100));
		VBoxGauche.setAlignment(Pos.CENTER);
		
		BorderPane bPGauche = new BorderPane();
		
		
		Button btnScene = new Button("Scene");
		Button btnStand = new Button("Stand");
		Button btnPtEau = new Button("Pt-Eau");
		
		btnScene.setOnAction(e->{
			choix=11;
			setList();
		});
		btnStand.setOnAction(e->{
			choix=12;
			setList();
		});
		btnPtEau.setOnAction(e->{
			choix=13;
			setList();
		});
		modifier.setOnAction(e->{
			if (liste1View.getSelectionModel().getSelectedIndex()!=-1) {
//				fenetreAjoutElement.setParametre(liste1View.getSelectionModel().getSelectedIndex());
				Main.ouvrirFenetreAjoutElement();
			}
		});
		setList();
		
		HBox hBbtnTop = new HBox();
		
		hBbtnTop.setPrefWidth(300);
		btnStand.setPrefWidth(100);
		btnScene.setPrefWidth(100);
		btnPtEau.setPrefWidth(100);
		hBbtnTop.getChildren().setAll(btnScene, btnStand, btnPtEau);
		
		liste1View.setPrefHeight(500);
		
		HBox hBbtnBot = new HBox();
		hBbtnBot.setPrefWidth(300);
		modifier.setPrefWidth(75);
		ajouter.setPrefWidth(75);
		supprimer.setPrefWidth(75);
		details.setPrefWidth(75);
		hBbtnBot.getChildren().setAll(ajouter, details, modifier, supprimer);
		griserBouton();
		modifier.setOnAction(e->griserBouton());
		ajouter.setOnAction(e->griserBouton());
		supprimer.setOnAction(e->griserBouton());
		details.setOnAction(e->griserBouton());
		
		bPGauche.setTop(hBbtnTop);
		bPGauche.setBottom(hBbtnBot);
		bPGauche.setCenter(liste1View);

		ajouter.setOnAction(e->{choix = 33; ajouterElement(); setList(); FenPlacement.ajouterImageScene();});
		FenPlacement.ajouterImageScene();
		
		VBoxGauche.setAlignment(Pos.CENTER);
		
		VBoxDroite.getChildren().add(bPGauche);
		AnchorPane.setTopAnchor(VBoxGauche, 20.0);
		AnchorPane.setLeftAnchor(VBoxGauche, 200.0);
		
		AnchorPane.setTopAnchor(VBoxDroite, 20.0);
		AnchorPane.setRightAnchor(VBoxDroite, 200.0);
		centreAp.getChildren().addAll(VBoxDroite, VBoxGauche);

		racine.setCenter(centreAp);
		
		
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
	
		
		return racine;	
	}
	private Button ajouter = new Button("ajouter");
	private Button modifier = new Button("modifier");
	private Button details = new Button("details");
	private Button supprimer = new Button("supprimer");
	public void griserBouton() {
		if(liste1View.getSelectionModel().getSelectedIndex()==-1) {
			this.modifier.setDisable(true);
			this.details.setDisable(true);
			this.supprimer.setDisable(true);
		}else {
			this.modifier.setDisable(false);
			this.details.setDisable(false);
			this.supprimer.setDisable(false);
		}
	}
	
	@SuppressWarnings("static-access")
	public void ajoutListe(String s) {
		this.liste1.add("Nom de l objet" + s);
		FenPlacement.liste1View.setItems(liste1);
	}
	@SuppressWarnings("static-access")
	public void modifierListe(String s, int index) {
		this.liste1.set(index, s);
		this.liste1View.setItems(liste1);
	}
	
	
	
	public static void setList() {
		int i;
		int index;
		if (place!=null) {
				switch (choix) {
				case 11:
					i = place.getArrayScene().size();
					index = 0;
					liste1.clear();
					while (index<i) {
						
						liste1.add(place.getArrayScene().get(index).getNom());
						index = index+1;
					}
					liste1View.setItems(liste1);
					break;
				case 12:
					i = place.getArrayStand().size();
					index = 0;
					while (index<i) {
						liste1.add(String.valueOf(place.getArrayStand().get(index).getNumStand()));
						index = index+1;
					}
					liste1View.setItems(liste1);
					break;
				case 13:
					i = place.getArrayPtEau().size();
					index = 0;
					while (index<i) {
						liste1.add(String.valueOf(index));
						index = index+1;
					}
					liste1View.setItems(liste1);
					break;
				case 21:
					
					break;
				case 22:
					
					break;	
				case 23:
					
					break;
				default:
					break;
				}
		}
		
	}
	public static int getChoix() {
		return choix;
	}

	public static void setChoix(int choix) {
		FenPlacement.choix = choix;
	}

	public void ajouterElement() {
		Main.ouvrirFenetreAjoutElement();
		
	}

	public static Placement getPlace() {
		return place;
	}

	public static void setPlace(Placement place) {
		FenPlacement.place = place;
	}
	public static void ajouterImageScene() {
		int i = 0;
		sPCentral.getChildren().clear();
		sPCentral.getChildren().addAll(rectCentral,aPCentral);
		moduleGestion.Scene sc;
		AnchorPane ac = new AnchorPane();
		while(i<place.getArrayScene().size()) {
			sc = place.getArrayScene().get(i);
			Image imgScene = new Image("scene.png", sc.getLongueur(),sc.getLargeur(),true,true);
			ImageView imgSceneView = new ImageView(imgScene);
			AnchorPane.setTopAnchor(imgSceneView, (double) (sc.getLatitude()*10));
			AnchorPane.setRightAnchor(imgSceneView, (double) (sc.getLongitude()*10));
			ac.getChildren().add(imgSceneView);
			i++;
		}
		sPCentral.getChildren().add(ac);
	}
}
