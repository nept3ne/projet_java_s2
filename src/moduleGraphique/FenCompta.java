package moduleGraphique;

import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FenCompta extends Stage {
		//Autres constantes
		private final int hauteurRectangleSup=70; // defini la hauteur de tous les rectnagles superieurs
		private final int largeurRectangleSup=300; // defini la largeur de chaque rectangles superieurs
		private final int hauteurRectangleInf=70;// defini la hauteur de tous les rectnagles inferieur
		private final int largeurRectangleInf=300;// defini la largeur de chaque rectangles inferieur

		private final int largStrokeBot=2; // defini l'epaisseur des bordures des rectangles inferieurs
		private final int largStrokTop=2; // defini l'epaisseur des bordures des rectangles superieur
		//formes pour la partie superieur du borderPane
		private Rectangle rectTop1 = new Rectangle();
		private Rectangle rectTop2 = new Rectangle();
		private Rectangle rectTop3 = new Rectangle();
		private Text txtTop3 = new Text();

		// formes pour la partie inferieur du borderPane 
		private Rectangle rectBot1 = new Rectangle();
		private Rectangle rectBot2 = new Rectangle();
		private Rectangle rectBot3 = new Rectangle();
		private Button butBot3 = new Button("Quitter");
		private final int hauteurBoutonBut3 = 30;
		private final int largeurBoutonBut3 = 100;
		private final int largeurRectangleBot_Cent = 500; // largeur du rectangle inferieur centre
		private final int hauteurRectangleBot_Cent = 50; // hauteur du rectangle inferieur centre
		private Text textBot2My  = new Text("Maryne G");
		private Text textBot2Ma = new Text("Matthis G");
		private Text textBot2Ql = new Text("Quentin L");
		private Text textBot2Cd = new Text("Corentin DR");
	
		public FenCompta() {

			this.setTitle("Festiv'Org");
			this.setResizable(false);
			this.setX(200);
			this.setY(200);
			Scene SceneAcc = new Scene(accContenu());
			this.setScene(SceneAcc);
			this.setResizable(false);
			this.setWidth(1000);
			this.setHeight(600);
			this.sizeToScene();
		}
		private Parent accContenu() {
	// --------------------------------------------------------------------------------------------------------------------------------------------------
			// ----------------------------- Ajustements des rectangles superieurs ----------------------------------------------------------------------
			HBox topRect = new HBox();
			StackPane topStackPane = new StackPane();		

			rectTop1.setFill(Color.GREY);
			rectTop2.setFill(Color.GREY); // remplis les rectangles en gris
			rectTop3.setFill(Color.GREY);


			rectTop1.setWidth(largeurRectangleSup);
			rectTop2.setWidth(largeurRectangleSup); // fixe la largeur des rectangles
			rectTop3.setWidth(largeurRectangleSup);

			rectTop1.setHeight(hauteurRectangleSup);
			rectTop2.setHeight(hauteurRectangleSup); // fixe la hauteur des rectangles superieurs
			rectTop3.setHeight(hauteurRectangleSup);

			rectTop1.setStroke(Color.BLACK);
			rectTop2.setStroke(Color.BLACK); // fixe la couleur des rectangles tops
			rectTop3.setStroke(Color.BLACK);

			rectTop1.setStrokeWidth(largStrokTop);
			rectTop2.setStrokeWidth(largStrokTop); // fixe l'epaisseur des recrangles 
			rectTop3.setStrokeWidth(largStrokTop);

			txtTop3.setText("Festiv'Org"); // fixe le texte superieur
			topStackPane.getChildren().addAll(rectTop2, txtTop3);

			topRect.getChildren().addAll(rectTop1, topStackPane, rectTop3);

			//-----------------------------------------------------------------------------------------------------------------------------------------------------
			//----------------------------Ajustement des rectangles inferieurs-----------------------------------------------------------------------------

			HBox botRect1 = new HBox();

			FlowPane nomPane = new FlowPane(Orientation.VERTICAL);
			nomPane.setAlignment(Pos.CENTER);
			nomPane.setColumnHalignment(HPos.CENTER);

			nomPane.setPrefWidth(largeurRectangleBot_Cent);
			nomPane.setPrefHeight(hauteurRectangleBot_Cent);
			nomPane.getChildren().addAll(textBot2My, textBot2Ma, textBot2Ql, textBot2Cd);



			rectBot1.setFill(Color.GREY);
			rectBot2.setFill(Color.GREY); // fixe la couleur des rectangles inferieur
			rectBot3.setFill(Color.GREY);

			rectBot1.setWidth(largeurRectangleInf);
			rectBot2.setWidth(largeurRectangleInf); // fixe la largeur du rectangle inferieur
			rectBot3.setWidth(largeurRectangleInf);

			rectBot1.setHeight(hauteurRectangleInf);
			rectBot2.setHeight(hauteurRectangleInf); // fixe la la hauteur du rectangle inferieur
			rectBot3.setHeight(hauteurRectangleInf);

			rectBot1.setStroke(Color.BLACK);
			rectBot2.setStroke(Color.BLACK); // met la couleur des rectangles inferieurs
			rectBot3.setStroke(Color.BLACK);

			rectBot1.setStrokeWidth(largStrokeBot);
			rectBot2.setStrokeWidth(largStrokeBot); // fixe la largeur des bordures des rectangles 
			rectBot3.setStrokeWidth(largStrokeBot);
			
			butBot3.setPrefWidth(largeurBoutonBut3);
			butBot3.setPrefHeight(hauteurBoutonBut3);

			StackPane btnStack = new StackPane();
			btnStack.setPrefWidth(hauteurRectangleInf);
			btnStack.setMaxWidth(largeurRectangleInf);
			btnStack.getChildren().addAll(rectBot3, butBot3);
			
			butBot3.setOnAction(e->{
				this.close();
			});
			
			StackPane nomStack = new StackPane();
			
			nomStack.setPrefWidth(largeurRectangleBot_Cent);
			nomStack.setMaxWidth(hauteurRectangleBot_Cent);
			nomStack.getChildren().addAll(rectBot2, nomPane);
			
			botRect1.getChildren().addAll(rectBot1,nomStack,btnStack);
			BorderPane racine = new BorderPane();
			racine.setTop(topRect);
			racine.setBottom(botRect1);
			
			
			return racine;
	}
}